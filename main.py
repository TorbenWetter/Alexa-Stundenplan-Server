import json

from flask import Flask

lessons = {
    "montag": {
        1: "Biologie",
        2: "Biologie",
        3: "Englisch",
        4: "Geschichte",
        5: "Mathe",
        6: "Mathe",
        7: "Informatik"
    },
    "dienstag": {
        1: "Chemie",
        2: "Erdkunde",
        3: "Sozialkunde",
        4: "Deutsch",
        5: "Physik",
        6: "Latein",
        7: "Informatik"
    },
    "mittwoch": {
        1: "Musik",
        2: "Musik",
        3: "Mathe",
        4: "Sport",
        5: "Deutsch",
        6: "Latein",
        7: "Religion"
    },
    "donnerstag": {
        2: "Sport",
        3: "Latein",
        4: "Englisch",
        5: "Deutsch",
        6: "Geschichte"
    },
    "freitag": {
        1: "Erdkunde",
        2: "Mathe",
        3: "Physik",
        4: "Englisch",
        5: "Chemie",
        6: "Religion"
    }
}

app = Flask(__name__)


@app.route("/<string:day>/")
def by_day(day):
    try:
        return json.dumps(lessons[day])
    except:
        return ":("


@app.route("/<string:day>/<int:lesson>/")
def by_lesson(day, lesson):
    try:
        return json.dumps(lessons[day][lesson])
    except:
        return ":("


if __name__ == "__main__":
    app.run()
